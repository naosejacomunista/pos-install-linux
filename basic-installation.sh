#!/bin/bash
apt update && 
apt upgrade && 
apt install -y xserver-xorg-video-intel mesa-utils mint-meta-codecs i965-va-driver gstreamer1.0-vaapi vainfo vlc browser-plugin-vlc build-essential nodejs docker git && wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add - && echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee /etc/apt/sources.list.d/google-chrome.list && sudo apt update && sudo apt install -y google-chrome-stable && chmod +x /usr/local/bin/docker-machine && curl -L https://github.com/docker/machine/releases/download/v0.15.0/docker-machine-`uname -s`-`uname -m` > /usr/local/bin/docker-machine && chmod +x /usr/local/bin/docker-machine

git clone https://github.com/horst3180/arc-theme --depth 1 && cd arc-theme
apt install libgtk-3-dev
./autogen.sh --prefix=/usr
make install

add-apt-repository ppa:oranchelo/oranchelo-icon-theme
apt update
apt-get install oranchelo-icon-theme

wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb

sudo apt-get install apt-transport-https
sudo apt-get update
sudo apt-get install dotnet-sdk-2.1